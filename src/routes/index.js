const router = require('express').Router();
const testRouter = require('./testToComplete').router;
const webServiceRouter = require('./webService').router;
router.use('/test', testRouter); 
router.use('/webservice', webServiceRouter); 

router.get('/', function (req, res) { 
  res.render('info', {title: 'Welcome to the datamaran test'});
})

router.get('*', function (req, res) {
  res.redirect('/');
});

module.exports = router;