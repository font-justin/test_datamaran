const router = require('express').Router();
// here the test begings
// get request to http://localhost:5000/webservice  will return data of customer and employe
var data ={} // data to pass to the view
router.get('/', function (req, res) {
    res.render('test', {data}); // here we render the view with the data, we use swig as template engine
  })
  module.exports = {
    router
  };